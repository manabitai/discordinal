extern crate proc_macro;

use proc_macro::TokenStream;
use quote::{quote, ToTokens, __private::Span};
use syn::{
    parse_macro_input, Data, DeriveInput, Field, Fields, GenericArgument, Ident, PathArguments,
    Type, TypePath,
};

fn is_option(type_path: &TypePath) -> bool {
    type_path.path.segments.iter().any(|s| s.ident == "Option")
}

fn extract_type_from_option(type_path: &TypePath) -> &Type {
    let type_params = &type_path.path.segments.iter().next().unwrap().arguments;
    let generic = match type_params {
        PathArguments::AngleBracketed(params) => params.args.iter().next().unwrap(),
        _ => panic!(),
    };
    match generic {
        GenericArgument::Type(ty) => ty,
        _ => panic!(),
    }
}

fn deprecation(field: &mut Field) -> syn::__private::TokenStream2 {
    field
        .attrs
        .iter()
        .any(|attr| match attr.path.get_ident() {
            Some(ident) => *ident == "deprecated",
            None => false,
        })
        .then_some(quote! { #[deprecated] })
        .unwrap_or(quote!())
}

/// Takes a struct field and returns a setter method as a TokenStream
fn map_field_to_fn(field: &mut Field) -> syn::__private::TokenStream2 {
    let name = field.ident.to_owned().unwrap();
    let deprecated = deprecation(field);

    let (ty, assign) = match field.ty.to_owned() {
        Type::Path(path) if is_option(&path) => {
            let generic = extract_type_from_option(&path);
            (generic.to_token_stream(), quote!(Some(#name)))
        }
        other => (other.to_token_stream(), quote!(#name)),
    };

    quote! {
        #deprecated
        pub fn #name(mut self, #name: #ty) -> Self {
            self.#name = #assign;
            self
        }
    }
}

/// Similar to the builder pattern, implements setter methods for all struct fields.
///
/// Unlike with the builder pattern, this macro does not create a separate `Builder`
/// struct that implements a `build()` method. This is done because the intention is
/// to use the struct itself, rather than a `build` target.
///
/// ```
/// #[derive(Setters)]
/// struct Message {
///     pub content: Option<String>,
///     pub nonce: i32,
/// }
/// ```
///
/// Expands to
///
/// ```
/// impl Message {
///     pub fn content<T>(mut self, content: T) -> Self
///     where
///         T: Into<String>,
///     {
///         self.content = Some(content.into());
///         self
///     }
///
///     pub fn nonce<T>(mut self, nonce: T) -> Self
///     where
///         T: Into<i32>,
///     {
///         self.nonce = nonce.into();
///         self
///     }
/// }
/// ```
///
/// # Examples
///
/// Recommended to pair this with the [`Default`] trait.
///
/// ```
/// use discordinal_derive::Setters;
///
/// #[derive(Default, Setters)]
/// struct Message {
///     pub content: Option<String>,
///     pub nonce: i32,
/// }
///
/// impl Message {
///     pub fn send(&self) {
///         // ...some implementation
///     }
/// }
///
/// let msg = Message::default()
///     .content("ping!".to_owned())
///     .nonce(0)
///     .send();
/// ```
#[proc_macro_derive(Setters)]
pub fn gen_setters(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);

    let name = input.ident;

    let generics = input.generics;
    let (impl_generics, ty_generics, where_generics) = generics.split_for_impl();

    let data = match input.data {
        Data::Struct(data) => data,
        _ => panic!("Can only be derived for structs"),
    };

    let mut fields = match data.fields {
        Fields::Named(fields) => fields,
        _ => panic!("Can only be derived for structs with named fields"),
    };

    let setters = fields.named.iter_mut().map(map_field_to_fn);

    let expanded = quote! {
        #[automatically_derived]
        impl #impl_generics #name #ty_generics #where_generics {
            #(#setters)*
        }
    };

    TokenStream::from(expanded)
}

#[proc_macro_derive(Partial)]
pub fn partial(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let ident = input.ident;
    let new_ident = Ident::new(&format!("Partial{ident}"), Span::call_site());

    let generics = input.generics;
    let (_, ty_generics, where_generics) = generics.split_for_impl();

    let data = match input.data {
        Data::Struct(data) => data,
        _ => panic!("Can only be derived for structs"),
    };

    let mut should_serialize = quote!();
    for attr in input.attrs {
        if let Some(ident) = attr.path.get_ident() {
            if ident == "partial_serialize" {
                should_serialize = quote!(Serialize);
            }
        }
    }

    let mut fields = match data.fields {
        Fields::Named(fields) => fields,
        _ => panic!("Can only be derived for structs with named fields"),
    };

    let fields = fields.named.iter_mut().map(|field| {
        let field_type = match &field.ty {
            Type::Path(path) => {
                if is_option(path) {
                    quote!(#path)
                } else {
                    quote!(Option<#path>)
                }
            }
            other => quote!(Option<#other>),
        };

        let name = &field.ident;

        quote! { pub #name: #field_type, }
    });

    let expanded = quote! {
        #[automatically_derived]
        #[derive(Debug, Deserialize, #should_serialize)]
        pub struct #new_ident #ty_generics #where_generics {
            #(#fields)*
        }
    };

    TokenStream::from(expanded)
}

/// This macro is used to indicate to [`Partial`] derive macro,
/// that the partial struct should implement [`Serialize`]
#[proc_macro_attribute]
pub fn partial_serialize(_: TokenStream, input: TokenStream) -> TokenStream {
    input
}
