use discordinal::{
    client::{Client, Intent},
    http::prelude::*,
    models::prelude::*,
    ClientBuilder, Event,
};

#[tokio::main]
async fn main() {
    dotenv::dotenv().ok();

    let token = dotenv::var("DISCORD_TOKEN").unwrap();

    let (mut client, ready_event) = ClientBuilder::default()
        .set_token(&token)
        .set_intents(vec![Intent::MessageContent, Intent::GuildMessages])
        .login()
        .await;

    println!("Ready {ready_event:#?}");

    loop {
        match client.recv_event().await {
            None => continue,
            Some(event) => match event {
                Event::MessageCreate(msg) => {
                    tokio::spawn(handle_message(client.clone(), msg));
                }
                Event::Ready(_) => {}
                _ => {}
            },
        }
    }
}

async fn handle_message(client: Client, message: Message) {
    if message.content == Some("!ping".to_string()) {
        let opts = MessageCreateOptions::default().content("!pong");
        create_message(&client.http, &message.channel_id, &opts)
            .await
            .ok();
    }
}
