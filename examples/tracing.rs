//! This example shows how to set up a tracing subscriber in order to view logs

use discordinal::ClientBuilder;
use tracing_subscriber::prelude::*;

#[tokio::main]
async fn main() {
    dotenv::dotenv().ok();

    let token = dotenv::var("DISCORD_TOKEN").unwrap();

    // Create a tracing filter so that only logs from discordinal are outputted
    let tracing_filter = tracing_subscriber::filter::Targets::new()
        .with_target("discordinal", tracing::Level::DEBUG);

    // Create a global subscriber to record and output logs from discordinal
    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(tracing_filter)
        .init();

    ClientBuilder::default().set_token(&token).login().await;
}
