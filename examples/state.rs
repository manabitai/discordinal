use discordinal::client::{Client, Intent};
use discordinal::http::prelude::*;
use discordinal::models::prelude::*;
use discordinal::{ClientBuilder, Event};

use std::sync::Arc;
use tokio::sync::Mutex;

#[derive(Default)]
struct AppState {
    pub counter: Arc<Mutex<usize>>,
}

#[tokio::main]
async fn main() {
    dotenv::dotenv().unwrap();

    let token = dotenv::var("DISCORD_TOKEN").unwrap();
    let state = AppState::default();

    let (mut client, ready_event) = ClientBuilder::default()
        .set_token(&token)
        .set_intents(vec![Intent::MessageContent, Intent::GuildMessages])
        .with_state(state)
        .login()
        .await;

    println!(
        "Logged in as {}#{}",
        ready_event.user.username, ready_event.user.discriminator
    );

    loop {
        if let Some(Event::MessageCreate(msg)) = client.recv_event().await {
            tokio::spawn(handle_message(client.clone(), msg));
        }
    }
}

async fn handle_message(client: Client<AppState>, message: Message) {
    if message.content == Some("!count".into()) {
        let mut counter = client.state.counter.lock().await;
        *counter += 1;

        let content = format!("Count is {counter}");

        let opts = MessageCreateOptions::default().content(&content);

        create_message(&client.http, &message.channel_id, &opts)
            .await
            .unwrap();
    }
}
