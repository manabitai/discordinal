use crate::{
    http::{request, Http},
    models::{
        codes::CloseEventCode,
        payload::{ready::Ready, Dispatch, Payload},
    },
};
use futures_util::{
    stream::{SplitSink, SplitStream},
    SinkExt, StreamExt,
};

use std::sync::Arc;
use std::time::{Duration, Instant};
use tokio::net::TcpStream;
use tokio::sync::{mpsc, Mutex};
use tokio_tungstenite::{
    tungstenite::{protocol::CloseFrame, Message},
    MaybeTlsStream, WebSocketStream,
};
use tracing::{debug, debug_span, error, warn};

type WsRead = SplitStream<WebSocketStream<MaybeTlsStream<TcpStream>>>;
type WsWrite = SplitSink<WebSocketStream<MaybeTlsStream<TcpStream>>, Message>;

/// Message to send to the ws_write tokio thread
#[derive(Debug)]
enum WriterMessage {
    /// Request from Discord to send a Heartbeat
    HeartbeatRequest,
    /// Acknowledgement from Discord that a Heartbeat was sent
    HeartbeatACK,
    /// New sequence number to send in Heartbeat payload
    Sequence(i64),
    /// Try to reconnect to gateway
    Reconnect(String, String),
    /// Close websocket connection
    Disconnect,
}

/// Main entry point for a Discord bot
pub struct Client<S = ()> {
    pub token: String,
    pub intents: i32,
    pub state: Arc<S>,
    pub http: Arc<Http>,
    ws_read: Arc<Mutex<WsRead>>,
    heartbeat_channel: Arc<mpsc::UnboundedSender<WriterMessage>>,
    resume_gateway_url: String,
    session_id: String,
}

impl<S> Clone for Client<S> {
    fn clone(&self) -> Self {
        Self {
            token: self.token.clone(),
            intents: self.intents,
            state: self.state.clone(),
            http: self.http.clone(),
            ws_read: self.ws_read.clone(),
            heartbeat_channel: self.heartbeat_channel.clone(),
            resume_gateway_url: self.resume_gateway_url.clone(),
            session_id: self.session_id.clone(),
        }
    }
}

impl<S> Client<S> {
    pub fn logout(&mut self) {
        self.heartbeat_channel
            .send(WriterMessage::Disconnect)
            .unwrap();
    }

    async fn handshake(
        token: &str,
        intents: i32,
        ws_read: &mut WsRead,
        ws_write: &mut WsWrite,
    ) -> Ready {
        debug!("performing handshake with Discord gateway");

        let payload = serde_json::json!({
            "op": 2,
            "d": {
                "token": token,
                "properties": {
                    "os": ::std::env::consts::OS,
                    "browser": "Disocrd library for Rust lang",
                    "device": "discordinal",
                },
                "compress": None::<String>,
                "large_threshold": None::<bool>,
                "shard": vec![0, 1],
                "presence": None::<String>,
                "intents": intents,
            },
        });

        let msg = Message::text(
            serde_json::to_string(&payload).expect("Failed to serialize Identify payload"),
        );

        debug!("sending Identify to gateway");

        ws_write
            .send(msg)
            .await
            .expect("Failed to send Identify to Discord gateway");

        let ready_event = ws_read
            .next()
            .await
            .expect("Did not receive Ready event")
            .expect("Unexpected error when receiving Ready event");

        let payload = serde_json::from_str::<Payload>(&ready_event.to_string())
            .expect("Failed to deserialize payload");

        match payload {
            Payload::Dispatch(_, event) => match *event {
                Dispatch::Ready(ready) => {
                    debug!("received Ready event");
                    *ready
                }
                _ => unreachable!("Expected to always receive Ready"),
            },
            Payload::InvalidSession(_) => panic!("max_concurrency limit exceeded"),
            _ => panic!("received unexpected payload: {:#?}", ready_event),
        }
    }

    async fn keep_alive(
        heartbeat_interval: i32,
        mut ws_write: WsWrite,
        mut rx: mpsc::UnboundedReceiver<WriterMessage>,
    ) {
        let mut last_heartbeat = Instant::now();
        let mut last_heartbeat_ack = Instant::now();
        // TODO add jitter to interval
        let heartbeat_interval = Duration::from_millis(heartbeat_interval as u64);
        let mut last_sequence = 0;

        loop {
            let mut should_heartbeat = last_heartbeat.elapsed() >= heartbeat_interval;

            if let Ok(msg) = rx.try_recv() {
                match msg {
                    WriterMessage::HeartbeatRequest => should_heartbeat = true,
                    WriterMessage::HeartbeatACK => last_heartbeat_ack = Instant::now(),
                    WriterMessage::Sequence(s) => last_sequence = s,
                    WriterMessage::Reconnect(token, session_id) => {
                        Self::reconnect((token, session_id, last_sequence), &mut ws_write).await;
                    }
                    WriterMessage::Disconnect => {
                        ws_write.close().await.ok();
                        break;
                    }
                }
            }

            // If no HeartbeatACK is received in heartbeat_interval + 4 secs
            // after sending a Heartbeat, then close connection and reconnect
            if last_heartbeat_ack.elapsed() >= heartbeat_interval + Duration::from_secs(4) {
                warn!("Did not receive HeartbeatACK in time, will attempt to reconnect");
                todo!("Handle not receiving HeartbeatACK");
            }

            if should_heartbeat {
                let payload = serde_json::json!({
                      "op": 1,
                      "d": last_sequence,
                });

                let msg = Message::text(
                    serde_json::to_string(&payload).expect("Failed to serialize Heartbeat payload"),
                );

                ws_write.send(msg).await.expect("Failed to send Heartbeat");

                debug!(seq = %last_sequence, "heartbeat sent");

                last_heartbeat = Instant::now();
            }
        }
    }

    async fn reconnect(data: (String, String, i64), writer: &mut WsWrite) {
        debug!("attempting to reconnect to gateway");

        let json = serde_json::json!({
            "op": 6,
            "d": {
                "token": data.0,
                "session_id": data.1,
                "seq": data.2,
            },
        });

        let msg = Message::text(
            serde_json::to_string(&json).expect("Failed to serialize Resume payload"),
        );

        writer
            .send(msg)
            .await
            .expect("Failed to send Resume payload");
    }

    pub async fn recv_event(&mut self) -> Option<Dispatch> {
        macro_rules! tx_send {
            ($event:expr) => {
                self.heartbeat_channel.send($event).expect(&format!(
                    "Failed to send message to heartbeat thread: {:#?}",
                    $event
                ));
            };
            ($event:expr,$return:expr) => {{
                tx_send!($event);
                $return
            }};
        }

        let mut guard = self.ws_read.lock().await;
        let event = guard.next().await;
        drop(guard);

        match event {
            Some(msg) => match msg {
                Ok(Message::Text(msg)) => match serde_json::from_str::<Payload>(&msg).expect("Failed to deserialize payload") {
                    Payload::Dispatch(s, event) => {
                        debug!(seq = s, "received Dispatch event");
                        tx_send!(WriterMessage::Sequence(s), Some(*event))
                    }
                    // If received Heartbeat, immediately request sending heartbeat to gateway
                    Payload::Heartbeat => {
                        debug!("received Heartbeat request");
                        tx_send!(WriterMessage::HeartbeatRequest, None)
                    },
                    // If received HeartbeatACK, send to heartbeat thread
                    Payload::HeartbeatACK => {
                        debug!("received HeartbeatACK");
                        tx_send!(WriterMessage::HeartbeatACK, None)
                    },
                    // InvalidSession may be received after Resume
                    Payload::InvalidSession(may_reconnect) => {
                        debug!(may_reconnect, "received InvalidSession");
                        if may_reconnect {
                            tx_send!(WriterMessage::Reconnect(self.token.clone(), self.session_id.clone()));
                        }
                        None
                    }
                    _ => unreachable!("Hello event are only sent once, and should be received and handled in connection process")
                }
                Ok(Message::Close(Some(close_frame))) => {
                    warn!(?close_frame, "connection to gateway closed unexpectedly");
                    if self.handle_close(close_frame) {
                        tx_send!(WriterMessage::Reconnect(self.token.clone(), self.session_id.clone()));
                    }
                    None
                }
                Ok(Message::Close(None)) => todo!("Handle ws close without message"),
                Ok(other) => {
                    error!("Unexpected Message type: {:#?}", other);
                    None
                }
                Err(e) => {
                    println!("Error {:#?}", e);
                    None
                }
            },
            None => None,
        }
    }

    fn handle_close(&mut self, close_frame: CloseFrame) -> bool {
        let num = u16::from(close_frame.code);
        let close_code: Option<CloseEventCode> = num::FromPrimitive::from_u16(num);

        match close_code {
            Some(code) => code.may_reconnect(),
            None => todo!("gracefully shutdown"),
        }
    }
}

pub enum Intent {
    Guilds = 0,
    GuildMembers = 1,
    GuildBans = 2,
    GuildEmojisAndStickers = 3,
    GuildIntegrations = 4,
    GuildWebhooks = 5,
    GuildInvites = 6,
    GuildVoiceStates = 7,
    GuildPresences = 8,
    GuildMessages = 9,
    GuildMessageReactions = 10,
    GuildMessageTyping = 11,
    DirectMessages = 12,
    DirectMessageReactions = 13,
    DirectMessageTyping = 14,
    MessageContent = 15,
    GuildScheduledEvents = 16,
    AutoModerationConfiguration = 20,
    AutoModerationExecution = 21,
}

/// The starting point for creating a Discord bot
///
/// # Example
///
/// ```ignore
/// use discordinal::{ClientBuilder, client::Intent};
///
/// let (client, ready_event, http) = ClientBuilder::default()
///     .set_token("Bot <token>")
///     .set_intents(vec![Intent::MessageContent, Intent::GuildMessages])
///     .login();
/// ```
pub struct ClientBuilder<'a, S = ()> {
    token: Option<&'a str>,
    intents: i32,
    state: S,
}

impl<'a> Default for ClientBuilder<'a, ()> {
    fn default() -> Self {
        Self {
            token: None,
            intents: 0,
            state: (),
        }
    }
}

impl<'a, S> ClientBuilder<'a, S> {
    /// Must pass token as `Bot <token>`
    pub fn set_token(mut self, token: &'a str) -> Self {
        self.token = Some(token);
        self
    }

    pub fn set_intents(mut self, intents: Vec<Intent>) -> Self {
        let mut res = 0;
        for i in intents {
            res += 1 << i as i32;
        }
        self.intents = res;
        self
    }

    pub fn with_state<S2>(self, state: S2) -> ClientBuilder<'a, S2> {
        ClientBuilder {
            state,
            intents: self.intents,
            token: self.token,
        }
    }

    /// Establish a connection to the Discord gateway.
    /// Returns an http client with the bot credentials set,
    /// and a websocket read stream for receiving messages.
    ///
    /// # Panics
    ///
    /// Panics if client fails to connect to Discord gateway
    /// or receives unexpected response from Discord
    pub async fn login(self) -> (Client<S>, Ready) {
        let _span = debug_span!("Client::login");
        let token = self.token.expect("token not set");

        let http = Http::new(token).unwrap();

        debug!("sending GET to /gateway/bot");

        let json: serde_json::Value = request!(http, get, "/gateway/bot").unwrap();
        let ws_url = json["url"]
            .as_str()
            .expect("Could not get url from response");

        debug!("connecting to Discord gateway");

        let (ws_stream, _) =
            tokio_tungstenite::connect_async(format!("{}/?v=10&encoding=json", ws_url))
                .await
                .expect("Failed to connect to Discord Gateway");

        let (mut write, mut read) = ws_stream.split();

        debug!("waiting for Hello event");

        let hello_event = read
            .next()
            .await
            .expect("Did not receive Hello event")
            .expect("Unexpected error when receiving Hello event");

        let payload =
            serde_json::from_str::<Payload>(&hello_event.to_string()).unwrap_or_else(|e| {
                panic!(
                    "failed to deserialize payload, expected hello, received: {:?}, error: {:?}",
                    hello_event, e
                )
            });

        let heartbeat_interval = match payload {
            Payload::Hello { heartbeat_interval } => {
                debug!("received Hello event: {:?}", payload);
                heartbeat_interval
            }
            _ => panic!("Expected Hello event, received: {:#?}", payload),
        };

        let ready_event = Client::<()>::handshake(token, self.intents, &mut read, &mut write).await;
        let (tx, rx) = mpsc::unbounded_channel();

        debug!("spawning tokio task for sending websocket messages to gateway");

        tokio::spawn(Client::<()>::keep_alive(heartbeat_interval, write, rx));

        let client = Client {
            token: token.to_owned(),
            intents: self.intents,
            ws_read: Arc::new(Mutex::new(read)),
            http: Arc::new(http),
            state: Arc::new(self.state),
            heartbeat_channel: Arc::new(tx),
            resume_gateway_url: ready_event.resume_gateway_url.clone(),
            session_id: ready_event.session_id.clone(),
        };

        (client, ready_event)
    }
}
