use reqwest::{RequestBuilder, StatusCode};
use serde::{de::DeserializeOwned, Deserialize};
use serde_json::Value;
use tracing::debug;

pub type HttpResult<T> = std::result::Result<T, HttpError>;

#[derive(thiserror::Error, Debug)]
pub enum BuildError {
    #[error(r#"invalid token (expected "Bot XXX", found "{0}")"#)]
    InvalidToken(String),
    #[error("failed to build http client: {0}")]
    Build(reqwest::Error),
}

#[derive(thiserror::Error, Debug)]
pub enum HttpError {
    #[error("failed to send request: {0}")]
    Send(reqwest::Error),
    #[error("failed to convert response body to bytes: {0}")]
    Bytes(reqwest::Error),
    #[error("failed to deserialize response body: {0}")]
    Deserialize(serde_json::Error),
    #[error("Discord API error (status {0}, error {1})")]
    ApiError(u16, ApiError),
}

/// <https://discord.com/developers/docs/reference#error-messages>
#[derive(Debug, Deserialize)]
pub struct ApiError {
    pub code: u16,
    // TODO make this a proper struct/enum
    pub errors: Option<Value>,
    pub message: String,
}

impl std::fmt::Display for ApiError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.message)
    }
}

pub struct Http {
    pub(crate) client: reqwest::Client,
}

impl Http {
    pub fn new(token: &str) -> Result<Self, BuildError> {
        let mut headers = reqwest::header::HeaderMap::new();
        let token = reqwest::header::HeaderValue::from_str(token)
            .map_err(|_| BuildError::InvalidToken(token.to_owned()))?;
        headers.insert(reqwest::header::AUTHORIZATION, token);

        let client = reqwest::ClientBuilder::new()
            .default_headers(headers)
            .build()
            .map_err(BuildError::Build)?;

        Ok(Self { client })
    }

    #[inline]
    pub(crate) fn base_url(&self) -> &'static str {
        "https://discord.com/api/v10"
    }

    pub(crate) async fn send<T: DeserializeOwned>(&self, req: RequestBuilder) -> HttpResult<T> {
        let res = req.send().await.map_err(HttpError::Send)?;
        let status = res.status();

        match status {
            StatusCode::OK | StatusCode::CREATED => {
                let json = res.text().await.map_err(HttpError::Bytes)?;
                debug!("received API response {}", json);
                serde_json::from_str(&json).map_err(HttpError::Deserialize)
            }
            StatusCode::NO_CONTENT => serde_json::from_str("").map_err(HttpError::Deserialize),
            _ => {
                let status = status.as_u16();
                let json = res.text().await.map_err(HttpError::Bytes)?;
                debug!("received API error {}", json);
                let data = serde_json::from_str(&json).map_err(HttpError::Deserialize)?;
                Err(HttpError::ApiError(status, data))
            }
        }
    }
}

macro_rules! request {
    ($http:ident, $method:ident, $url:expr) => {{
        let url = format!("{}/{}", $http.base_url(), $url);
        let req = $http.client.$method(url);
        $http.send(req).await
    }};
    ($http:ident, $method:ident, $url:expr, body=$body:ident) => {{
        let url = format!("{}/{}", $http.base_url(), $url);
        let req = $http.client.$method(url).json($body);
        $http.send(req).await
    }};
}

pub(crate) use request;

pub mod prelude {
    pub use super::{
        application::*, application_role_connection_metadata::*, channel::*, emoji::*, guild::*,
        user::*, Http,
    };
}

pub mod application_command {
    use discordinal_derive::Setters;
    use serde::Serialize;

    use super::*;
    use crate::models::prelude::*;
    use std::collections::HashMap;

    #[derive(Default, Debug, Setters)]
    pub struct GetApplicationCommandsQuery {
        pub with_localizations: Option<bool>,
    }

    #[derive(Default, Debug, Serialize, Setters)]
    pub struct CreateApplicationCommandParams<'a> {
        pub name: &'a str,
        pub name_localizations: Option<HashMap<Locale, &'a str>>,
        pub description: Option<&'a str>,
        pub description_localizations: Option<HashMap<Locale, &'a str>>,
        pub options: Option<Vec<ApplicationCommandOption>>,
        pub default_member_permissions: Option<&'a str>,
        pub dm_permission: Option<bool>,
        #[serde(rename = "type")]
        pub command_type: Option<ApplicationCommandType>,
        pub nsfw: Option<bool>,
    }

    #[derive(Default, Debug, Serialize, Setters)]
    pub struct EditApplicationCommandParams<'a> {
        pub name: Option<&'a str>,
        pub name_localizations: Option<HashMap<Locale, &'a str>>,
        pub description: Option<&'a str>,
        pub description_localizations: Option<HashMap<Locale, &'a str>>,
        pub options: Option<ApplicationCommandOption>,
        pub default_member_permissions: Option<&'a str>,
        pub dm_permission: Option<bool>,
        pub nsfw: Option<bool>,
    }

    #[derive(Default, Debug, Serialize, Setters)]
    pub struct BulkOverwriteApplicationCommand<'a> {
        pub id: Option<&'a str>,
        pub name: &'a str,
        pub name_localizations: Option<HashMap<Locale, &'a str>>,
        pub description: &'a str,
        pub description_localizations: Option<HashMap<Locale, &'a str>>,
        pub options: Option<ApplicationCommandOption>,
        pub default_member_permissions: Option<&'a str>,
        pub dm_permission: Option<bool>,
        pub default_permission: Option<bool>,
        #[serde(rename = "type")]
        pub command_type: Option<ApplicationCommandType>,
        pub nsfw: Option<bool>,
    }

    #[derive(Default, Debug, Serialize, Setters)]
    pub struct EditApplicationCommandPermissionsParams<'a> {
        pub permissions: &'a [ApplicationCommandPermission],
    }

    pub async fn get_global_application_commands(
        http: &Http,
        application_id: &str,
        query: GetApplicationCommandsQuery,
    ) -> HttpResult<Vec<ApplicationCommand>> {
        let mut url = format!("applications/{application_id}/commands");
        if let Some(with_localizations) = query.with_localizations {
            url.push_str("?with_localizations=");
            url.push_str(&with_localizations.to_string());
        }
        request!(http, get, url)
    }

    pub async fn create_global_application_command(
        http: &Http,
        application_id: &str,
        params: &CreateApplicationCommandParams<'_>,
    ) -> HttpResult<ApplicationCommand> {
        let url = format!("applications/{application_id}/commands");
        request!(http, post, url, body = params)
    }

    pub async fn get_global_application_command(
        http: &Http,
        application_id: &str,
        command_id: &str,
    ) -> HttpResult<ApplicationCommand> {
        let url = format!("applications/{application_id}/commands/{command_id}");
        request!(http, get, url)
    }

    pub async fn edit_global_application_command(
        http: &Http,
        application_id: &str,
        command_id: &str,
        params: &EditApplicationCommandParams<'_>,
    ) -> HttpResult<ApplicationCommand> {
        let url = format!("applications/{application_id}/commands/{command_id}");
        request!(http, patch, url, body = params)
    }

    pub async fn delete_global_application_command(
        http: &Http,
        application_id: &str,
        command_id: &str,
    ) -> HttpResult<()> {
        let url = format!("applications/{application_id}/commands/{command_id}");
        request!(http, delete, url)
    }

    pub async fn bulk_overwrite_global_application_commands(
        http: &Http,
        application_id: &str,
        new_commands: &[BulkOverwriteApplicationCommand<'_>],
    ) -> HttpResult<ApplicationCommand> {
        let url = format!("applications/{application_id}/commands");
        request!(http, put, url, body = new_commands)
    }

    pub async fn get_guild_application_commands(
        http: &Http,
        application_id: &str,
        guild_id: &str,
        query: GetApplicationCommandsQuery,
    ) -> HttpResult<Vec<ApplicationCommand>> {
        let mut url = format!("applications/{application_id}/guilds/{guild_id}/commands");
        if let Some(with_localizations) = query.with_localizations {
            url.push_str("?with_localizations=");
            url.push_str(&with_localizations.to_string());
        }
        request!(http, get, url)
    }

    pub async fn create_guild_application_command(
        http: &Http,
        application_id: &str,
        guild_id: &str,
        params: &CreateApplicationCommandParams<'_>,
    ) -> HttpResult<ApplicationCommand> {
        let url = format!("applications/{application_id}/guilds/{guild_id}/commands");
        request!(http, post, url, body = params)
    }

    pub async fn get_guild_application_command(
        http: &Http,
        application_id: &str,
        guild_id: &str,
        command_id: &str,
    ) -> HttpResult<ApplicationCommand> {
        let url = format!("applications/{application_id}/guilds/{guild_id}/commands/{command_id}");
        request!(http, get, url)
    }

    pub async fn edit_guild_application_command(
        http: &Http,
        application_id: &str,
        guild_id: &str,
        command_id: &str,
        params: &EditApplicationCommandParams<'_>,
    ) -> HttpResult<ApplicationCommand> {
        let url = format!("applications/{application_id}/guilds/{guild_id}/commands/{command_id}");
        request!(http, patch, url, body = params)
    }

    pub async fn delete_guild_application_command(
        http: &Http,
        application_id: &str,
        guild_id: &str,
        command_id: &str,
    ) -> HttpResult<()> {
        let url = format!("applications/{application_id}/guilds/{guild_id}/commands/{command_id}");
        request!(http, delete, url)
    }

    pub async fn bulk_overwrite_guild_application_commands(
        http: &Http,
        application_id: &str,
        guild_id: &str,
        new_commands: &[BulkOverwriteApplicationCommand<'_>],
    ) -> HttpResult<ApplicationCommand> {
        let url = format!("applications/{application_id}/guilds/{guild_id}/commands");
        request!(http, put, url, body = new_commands)
    }

    pub async fn get_guild_application_command_permissions(
        http: &Http,
        application_id: &str,
        guild_id: &str,
    ) -> HttpResult<GuildApplicationCommandPermission> {
        let url = format!("applications/{application_id}/guilds/{guild_id}/commands/permissions");
        request!(http, get, url)
    }

    pub async fn get_application_command_permissions(
        http: &Http,
        application_id: &str,
        guild_id: &str,
        command_id: &str,
    ) -> HttpResult<GuildApplicationCommandPermission> {
        let url = format!("applications/{application_id}/guilds/{guild_id}/commands/{command_id}");
        request!(http, get, url)
    }

    pub async fn edit_application_command_permissions(
        http: &Http,
        application_id: &str,
        guild_id: &str,
        command_id: &str,
        params: &EditApplicationCommandPermissionsParams<'_>,
    ) -> HttpResult<GuildApplicationCommandPermission> {
        let url = format!(
            "applications/{application_id}/guilds/{guild_id}/commands/{command_id}/permissions"
        );
        request!(http, put, url, body = params)
    }
}

pub mod application {
    use super::*;
    use crate::models::prelude::*;

    pub async fn get_current_application(http: &Http) -> HttpResult<Application> {
        request!(http, get, "applications/@me")
    }
}

pub mod application_role_connection_metadata {
    use super::*;
    use crate::models::prelude::*;

    pub async fn get_application_role_connection_metadata_records(
        http: &Http,
        application_id: &str,
    ) -> HttpResult<Vec<ApplicationRoleConnectionMetadata>> {
        let url = format!("applications/{application_id}/role-connections/metadata");
        request!(http, get, url)
    }

    pub async fn update_application_role_connection_metadata_records(
        http: &Http,
        application_id: &str,
        params: &[ApplicationRoleConnectionMetadata],
    ) -> HttpResult<Vec<ApplicationRoleConnectionMetadata>> {
        let url = format!("applications/{application_id}/role-connections/metadata");
        request!(http, put, url, body = params)
    }
}

pub mod channel {
    use super::*;
    use crate::models::prelude::*;
    use discordinal_derive::Setters;
    use serde::Serialize;

    #[derive(Default, Debug, Serialize, Setters)]
    pub struct MessageCreateOptions<'a> {
        pub content: Option<&'a str>,
        pub nonce: Option<i32>,
        pub tts: Option<bool>,
        pub embeds: Vec<Embed>,
        //pub allowed_mentions: AllowedMentions,
        //pub message_reference: MessageReference,
        pub components: Option<Vec<ActionRow>>,
        pub sticker_ids: Option<Vec<&'a str>>,
        //pub files: ?
        pub payload_json: Option<&'a str>,
        pub attachments: Vec<PartialAttachment>,
        pub flags: i32,
    }

    pub async fn get_channel(http: &Http, channel_id: &str) -> HttpResult<Channel> {
        request!(http, get, format!("channels/{channel_id}"))
    }

    pub async fn create_message(
        http: &Http,
        channel_id: &str,
        options: &MessageCreateOptions<'_>,
    ) -> HttpResult<Message> {
        let url = format!("channels/{channel_id}/messages");
        request!(http, post, url, body = options)
    }
}

pub mod emoji {
    use super::*;
    use crate::models::prelude::*;
    use discordinal_derive::Setters;
    use serde::Serialize;

    #[derive(Default, Debug, Serialize, Setters)]
    pub struct CreateGuildEmojiParams<'a> {
        pub name: &'a str,
        pub image: &'a str,
        pub roles: &'a [&'a str],
    }

    #[derive(Default, Debug, Serialize, Setters)]
    pub struct ModifyGuildEmojiParams<'a> {
        pub name: &'a str,
        pub roles: Option<&'a [&'a str]>,
    }

    pub async fn list_guild_emojis(http: &Http, guild_id: &str) -> HttpResult<Vec<Emoji>> {
        request!(http, get, format!("guilds/{guild_id}/emojis"))
    }

    pub async fn get_guild_emoji(http: &Http, guild_id: &str, emoji_id: &str) -> HttpResult<Emoji> {
        request!(http, get, format!("guilds/{guild_id}/emojis/{emoji_id}"))
    }

    pub async fn create_guild_emoji(
        http: &Http,
        guild_id: &str,
        params: &CreateGuildEmojiParams<'_>,
    ) -> HttpResult<Emoji> {
        let url = format!("guilds/{guild_id}/emojis");
        request!(http, post, url, body = params)
    }

    pub async fn modify_guild_emoji(
        http: &Http,
        guild_id: &str,
        emoji_id: &str,
        params: &ModifyGuildEmojiParams<'_>,
    ) -> HttpResult<Emoji> {
        let url = format!("guilds/{guild_id}/emojis/{emoji_id}");
        request!(http, patch, url, body = params)
    }

    pub async fn delete_guild_emoji(http: &Http, guild_id: &str, emoji_id: &str) -> HttpResult<()> {
        request!(http, delete, format!("guilds/{guild_id}/emojis/{emoji_id}"))
    }
}

pub mod guild {
    use super::*;
    use crate::models::prelude::*;
    use discordinal_derive::Setters;
    use serde::Serialize;

    #[derive(Default, Debug, Serialize, Setters)]
    pub struct CreateGuildParams<'a> {
        pub name: &'a str,
        pub region: Option<&'a str>,
        pub icon: Option<&'a str>,
        pub verification_level: Option<VerificationLevel>,
        pub default_message_notifications: Option<DefaultMessageNotificationLevel>,
        pub explicit_content_filter: Option<ExplicitContentFilterLevel>,
        pub roles: Option<Vec<Role>>,
        pub channels: Option<Vec<PartialChannel>>,
        pub afk_channel_id: Option<&'a str>,
        pub afk_timeout: Option<u32>,
        pub system_channel_id: Option<&'a str>,
        pub system_channel_flags: Option<u32>,
    }

    #[derive(Default, Debug, Serialize, Setters)]
    pub struct GetGuildQuery {
        pub with_counts: Option<bool>,
    }

    #[derive(Default, Debug, Serialize, Setters)]
    pub struct ModifyGuildParams<'a> {
        pub name: Option<&'a str>,
        pub verification_level: Option<i32>,
        pub default_message_notifications: Option<i32>,
        pub explicit_content_filter: Option<i32>,
        pub afk_channel_id: Option<&'a str>,
        pub afk_timeout: Option<i32>,
        pub icon: Option<&'a str>,
        pub owner_id: Option<&'a str>,
        pub splash: Option<&'a str>,
        pub discovery_splash: Option<&'a str>,
        pub banner: Option<&'a str>,
        pub system_channel_id: Option<&'a str>,
        pub system_channel_flags: Option<i32>,
        pub rules_channel_id: Option<&'a str>,
        pub public_updates_channel_id: Option<&'a str>,
        pub preferred_locale: Option<&'a str>,
        pub features: Option<Vec<GuildFeature>>,
        pub description: Option<&'a str>,
        pub premium_progress_bar_enabled: Option<bool>,
        pub safety_alerts_channel_id: Option<&'a str>,
    }

    #[derive(Default, Debug, Serialize, Setters)]
    pub struct CreateGuildChannelParams<'a> {
        pub name: &'a str,
        #[serde(rename = "type")]
        pub channel_type: Option<ChannelType>,
        pub topic: Option<&'a str>,
        pub bitrate: Option<u32>,
        pub user_limit: Option<i32>,
        pub rate_limit_per_user: Option<i32>,
        pub position: Option<i32>,
        pub permission_overwrites: Option<PartialOverwrite>,
        pub parent_id: Option<&'a str>,
        pub nsfw: Option<bool>,
        pub rtc_region: Option<&'a str>,
        pub video_quality_mode: Option<i32>,
        pub default_auto_archive_duration: Option<i32>,
        pub default_reaction_emoji: Option<DefaultReaction>,
        pub available_tags: Option<Vec<Tag>>,
        pub default_sort_order: Option<i32>,
    }

    #[derive(Default, Debug, Serialize, Setters)]
    pub struct ModifyGuildChannelPositionParams<'a> {
        pub id: &'a str,
        pub position: Option<u16>,
        pub lock_permisions: Option<bool>,
        pub parent_id: Option<&'a str>,
    }

    pub async fn create_guild(http: &Http, params: &CreateGuildParams<'_>) -> HttpResult<Guild> {
        request!(http, post, "guilds", body = params)
    }

    pub async fn get_guild(
        http: &Http,
        guild_id: &str,
        query: &GetGuildQuery,
    ) -> HttpResult<Guild> {
        let mut url = format!("guilds/{guild_id}");
        if let Some(with_counts) = query.with_counts {
            url.push_str("?with_counts=");
            url.push_str(&with_counts.to_string());
        }
        request!(http, get, url)
    }

    pub async fn get_guild_preview(http: &Http, guild_id: &str) -> HttpResult<GuildPreview> {
        request!(http, get, format!("guilds/{guild_id}/preview"))
    }

    pub async fn modify_guild(
        http: &Http,
        guild_id: &str,
        params: &ModifyGuildParams<'_>,
    ) -> HttpResult<Guild> {
        request!(http, patch, format!("guilds/{guild_id}"), body = params)
    }

    pub async fn delete_guild(http: &Http, guild_id: &str) -> HttpResult<()> {
        request!(http, delete, format!("guilds/{guild_id}"))
    }

    pub async fn get_guild_channels(http: &Http, guild_id: &str) -> HttpResult<Vec<Channel>> {
        request!(http, get, format!("guilds/{guild_id}/channels"))
    }

    pub async fn creat_guild_channel(
        http: &Http,
        guild_id: &str,
        params: &CreateGuildChannelParams<'_>,
    ) -> HttpResult<Channel> {
        let url = format!("guilds/{guild_id}/channels");
        request!(http, post, url, body = params)
    }

    pub async fn modify_guild_channel_positions(
        http: &Http,
        guild_id: &str,
        params: &ModifyGuildChannelPositionParams<'_>,
    ) -> HttpResult<()> {
        let url = format!("guilds/{guild_id}/channels");
        request!(http, patch, url, body = params)
    }
}

pub mod user {
    use super::*;
    use crate::models::prelude::*;
    use discordinal_derive::Setters;
    use serde::Serialize;

    #[derive(Default, Debug, Serialize, Setters)]
    pub struct ModifyUserOptions<'a> {
        pub username: Option<&'a str>,
        pub avatar: Option<&'a str>,
    }

    #[derive(Default, Debug, Serialize, Setters)]
    pub struct GetCurrentUserGuildsQuery<'a> {
        pub before: Option<&'a str>,
        pub after: Option<&'a str>,
        pub limit: Option<u16>,
        pub with_counts: Option<bool>,
    }

    #[derive(Default, Debug, Serialize, Setters)]
    pub struct CreateDmOptions<'a> {
        pub recipient_id: &'a str,
    }

    #[derive(Default, Debug, Serialize, Setters)]
    pub struct CreateGroupDmOptions<'a> {
        pub access_tokens: Vec<&'a str>,
        /// a dictionary of user ids to their respective nicknames
        pub nicks: serde_json::Value,
    }

    #[derive(Default, Debug, Serialize, Setters)]
    pub struct UpdateUserApplicationRoleConnectionOptions<'a> {
        pub platform_name: Option<&'a str>,
        pub platform_username: Option<&'a str>,
        /// object mapping application role connection metadata keys to their string-ified value (max 100 characters) for the user on the platform a bot has connected
        pub nicks: serde_json::Value,
    }

    pub async fn get_current_user(http: &Http) -> HttpResult<User> {
        request!(http, get, "users/@me")
    }

    pub async fn get_user(http: &Http, user_id: &str) -> HttpResult<User> {
        request!(http, get, format!("users/{user_id}"))
    }

    pub async fn modify_current_user(
        http: &Http,
        options: &ModifyUserOptions<'_>,
    ) -> HttpResult<User> {
        request!(http, patch, "users/@me", body = options)
    }

    pub async fn get_current_user_guilds(
        http: &Http,
        query: GetCurrentUserGuildsQuery<'_>,
    ) -> HttpResult<Vec<PartialGuild>> {
        let mut url = String::from("users/@me/guilds");
        if query.before.is_some()
            || query.after.is_some()
            || query.limit.is_some()
            || query.with_counts.is_some()
        {
            url.push('?');
        }
        if let Some(before) = query.before {
            url.push_str("before=");
            url.push_str(before);
        }
        if let Some(after) = query.after {
            url.push_str("before=");
            url.push_str(after);
        }
        if let Some(limit) = query.limit {
            url.push_str("before=");
            url.push_str(&limit.to_string());
        }
        if let Some(with_counts) = query.with_counts {
            url.push_str("before=");
            url.push_str(&with_counts.to_string());
        }
        request!(http, get, "users/@me/guilds")
    }

    pub async fn get_current_user_guild_member(
        http: &Http,
        guild_id: &str,
    ) -> HttpResult<GuildMember> {
        request!(http, get, format!("users/@me/guilds/{guild_id}/member"))
    }

    pub async fn leave_guild(http: &Http, guild_id: &str) -> HttpResult<()> {
        request!(http, delete, format!("users/@me/guilds/{guild_id}"))
    }

    pub async fn create_dm(http: &Http, options: &CreateDmOptions<'_>) -> HttpResult<Channel> {
        request!(http, post, "users/@me/channels", body = options)
    }

    pub async fn create_group_dm(
        http: &Http,
        options: &CreateDmOptions<'_>,
    ) -> HttpResult<Channel> {
        request!(http, post, "users/@me/channels", body = options)
    }

    pub async fn get_user_connections(http: &Http) -> HttpResult<Vec<Connection>> {
        request!(http, get, "users/@me/connections")
    }

    pub async fn get_user_application_role_connection(
        http: &Http,
        application_id: &str,
    ) -> HttpResult<ApplicationRoleConnection> {
        request!(
            http,
            get,
            format!("users/@me/applications/{application_id}/role-connection")
        )
    }

    pub async fn update_user_application_role_connection(
        http: &Http,
        application_id: &str,
        options: &UpdateUserApplicationRoleConnectionOptions<'_>,
    ) -> HttpResult<()> {
        request!(
            http,
            put,
            format!("users/@me/applications/{application_id}/role-connection"),
            body = options
        )
    }
}
