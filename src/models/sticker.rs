use super::prelude::*;
use serde::Deserialize;
use serde_repr::Deserialize_repr;

#[derive(Debug, Deserialize)]
pub struct Sticker {
    pub id: String,
    pub pack_id: Option<String>,
    pub name: String,
    pub description: Option<String>,
    pub tags: String,
    pub asset: Option<String>,
    #[serde(rename = "type")]
    pub sticker_type: StickerType,
    pub format_type: FormatType,
    pub available: Option<bool>,
    pub guild_id: Option<String>,
    pub user: Option<User>,
    pub sort_value: Option<i32>,
}

#[derive(Debug, Deserialize_repr)]
#[repr(i8)]
pub enum StickerType {
    Standart = 1,
    Guild = 2,
}

#[derive(Debug, Deserialize_repr)]
#[repr(i8)]
pub enum FormatType {
    Png = 1,
    Apng = 2,
    Lottie = 3,
}

#[derive(Debug, Deserialize)]
pub struct StickerItem {
    pub id: String,
    pub name: String,
    pub format_type: FormatType,
}

#[derive(Debug, Deserialize)]
pub struct StickerPack {
    pub id: String,
    pub stickers: Vec<Sticker>,
    pub name: String,
    pub sku_id: String,
    pub cover_sticker_id: Option<String>,
    pub description: String,
    pub banner_asset_id: Option<String>,
}
