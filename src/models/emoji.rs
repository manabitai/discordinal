use super::prelude::*;
use discordinal_derive::{partial_serialize, Partial, Setters};
use serde::{Deserialize, Serialize};

#[derive(Debug, Default, Setters, Deserialize, Serialize, Partial)]
#[partial_serialize]
pub struct Emoji {
    pub id: Option<String>,
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub roles: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user: Option<User>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub require_colons: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub managed: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub animated: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub available: Option<bool>,
}
