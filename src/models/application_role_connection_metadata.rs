use super::prelude::*;
use serde::{Deserialize, Serialize};
use serde_repr::{Deserialize_repr, Serialize_repr};
use std::collections::HashMap;

#[derive(Debug, Serialize, Deserialize)]
pub struct ApplicationRoleConnectionMetadata {
    #[serde(rename = "type")]
    pub metadata_type: ApplicationRoleConnectionMetadataType,
    pub key: String,
    pub name: String,
    pub name_localizations: Option<HashMap<Locale, String>>,
    pub description: String,
    pub description_localizations: Option<HashMap<Locale, String>>,
}

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum ApplicationRoleConnectionMetadataType {
    IntegerLessThanOrEqual = 1,
    IntegerGreaterThanOrEqual = 2,
    IntegerEqual = 3,
    IntegerNotEqual = 4,
    DatetimeLessThanOrEqual = 5,
    DatetimeGreaterThanOrEqual = 6,
    BooleanEqual = 7,
    BooleanNotEqual = 8,
}
