use super::prelude::*;
use discordinal_derive::Partial;
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Partial)]
pub struct Application {
    pub id: String,
    pub name: String,
    pub icon: Option<String>,
    pub description: String,
    pub rpc_origins: Option<Vec<String>>,
    pub bot_public: bool,
    pub bot_require_code_grant: bool,
    pub terms_of_service_url: Option<String>,
    pub privacy_policy_url: Option<String>,
    pub owner: Option<PartialUser>,
    pub verify_key: String,
    pub team: Team,
    pub guild_id: Option<String>,
    pub guild: PartialGuild,
    pub primary_sku_id: Option<String>,
    pub slug: Option<String>,
    pub cover_image: Option<String>,
    pub flags: Option<i32>,
    pub tags: Option<Vec<String>>,
    pub install_params: Option<InstallParams>,
    pub custom_install_url: Option<String>,
    pub role_connections_verification_url: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub enum ApplicationFlag {
    ApplicationAutoModerationRuleCreateBadge = 6,
    GatewayPresence = 12,
    GatewayPresenceLimited = 13,
    GatewayGuildMembers = 14,
    GatewayGuildMembersLimited = 15,
    VerificationPendingGuildLimit = 16,
    Embedded = 17,
    GatewayMessageContent = 18,
    GatewayMessageContentLimited = 19,
    ApplicationCommandBadge = 23,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct InstallParams {
    pub scopes: Vec<String>,
    pub permissions: String,
}
