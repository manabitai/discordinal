use super::prelude::*;
use discordinal_derive::{partial_serialize, Partial};
use serde::{Deserialize, Serialize};
use serde_repr::{Deserialize_repr, Serialize_repr};

#[derive(Debug, Deserialize, Serialize, Partial)]
#[partial_serialize]
pub struct User {
    pub id: String,
    pub username: String,
    pub discriminator: String,
    pub global_name: Option<String>,
    pub avatar: Option<String>,
    pub bot: Option<bool>,
    pub system: Option<bool>,
    pub mfa_enabled: Option<bool>,
    pub banner: Option<String>,
    pub accent_color: Option<String>,
    pub locale: Option<String>,
    pub verified: Option<bool>,
    pub email: Option<String>,
    pub flags: Option<i32>,
    pub premium_type: Option<PremiumType>,
    pub public_flags: Option<i32>,
    pub avatar_decoration: Option<String>,
}

#[derive(Debug, Deserialize_repr, Serialize_repr)]
#[repr(i8)]
pub enum PremiumType {
    None = 0,
    NitroClassic = 1,
    Nitro = 2,
    NitroBasic = 3,
}

#[derive(Debug, Deserialize)]
pub struct Connection {
    pub id: String,
    pub name: String,
    #[serde(rename = "type")]
    pub connection_type: ConnectionType,
    pub revoked: Option<bool>,
    pub integrations: Option<PartialIntegration>,
    pub verified: bool,
    pub friend_sync: bool,
    pub show_activity: bool,
    pub two_way_link: bool,
    pub visibility: VisibilityType,
}

#[allow(non_camel_case_types)]
#[derive(Debug, Deserialize)]
pub enum ConnectionType {
    battlenet,
    ebay,
    epicgames,
    facebook,
    github,
    leagueoflegends,
    paypal,
    playstation,
    reddit,
    riotgames,
    sortify,
    skype,
    steam,
    twitch,
    twitter,
    xbox,
    youtube,
}

#[derive(Debug, Deserialize_repr)]
#[repr(i8)]
pub enum VisibilityType {
    None = 0,
    Everyone = 1,
}

#[derive(Debug, Deserialize)]
pub struct ApplicationRoleConnection {
    pub platform_name: Option<String>,
    pub platform_username: Option<String>,
    //pub metadata ?
}
