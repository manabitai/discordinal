use super::prelude::*;
use discordinal_derive::Partial;
use serde::{Deserialize, Serialize};
use serde_repr::{Deserialize_repr, Serialize_repr};

pub mod role;

#[derive(Debug, Deserialize, Partial)]
pub struct Guild {
    pub id: String,
    pub name: String,
    pub icon: Option<String>,
    pub icon_hash: Option<String>,
    pub splash: Option<String>,
    pub discovery_splash: Option<String>,
    /// This field is only sent when using the GET Current User Guilds endpoint and are relative to the requested user
    pub owner: Option<bool>,
    pub owner_id: String,
    /// This field is only sent when using the GET Current User Guilds endpoint and are relative to the requested user
    pub permissions: Option<String>,
    pub afk_channel_id: Option<String>,
    pub afk_timeout: i32,
    pub widget_enabled: Option<bool>,
    pub widget_channel_id: Option<String>,
    pub verification_level: i32,
    pub default_message_notifications: i32,
    pub explicit_content_filter: i32,
    pub roles: Vec<Role>,
    pub emojis: Vec<Emoji>,
    pub features: Vec<GuildFeature>,
    pub mfa_level: i32,
    pub application_id: Option<String>,
    pub system_channel_id: Option<String>,
    pub system_channel_flags: i32,
    pub rules_channel_id: Option<String>,
    pub max_presences: Option<i32>,
    pub max_members: Option<i32>,
    pub vanity_url_code: Option<String>,
    pub description: Option<String>,
    pub banner: Option<String>,
    pub premium_tier: i32,
    pub premium_subscription_count: Option<i32>,
    pub preferred_locale: String,
    pub public_updates_channel_id: Option<String>,
    pub max_video_channel_users: Option<i32>,
    pub max_stage_video_channel_users: Option<i32>,
    pub approximate_member_count: Option<i32>,
    pub approximate_presence_count: Option<i32>,
    pub welcome_screen: Option<WelcomeScreen>,
    pub nsfw_level: GuildNsfwLevel,
    pub stickers: Option<Vec<Sticker>>,
    pub premium_progress_bar_enabled: bool,
    pub safety_alerts_channel_id: Option<String>,
}

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum DefaultMessageNotificationLevel {
    AllMessages = 0,
    OnlyMentions = 1,
}

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum ExplicitContentFilterLevel {
    Disabled = 0,
    MembersWithoutRoles = 1,
    AllMembers = 2,
}

#[derive(Debug, Deserialize_repr)]
#[repr(u8)]
pub enum MfaLevel {
    None = 0,
    Elevated = 1,
}

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum VerificationLevel {
    None = 0,
    Low = 1,
    Medium = 2,
    High = 3,
    VeryHigh = 4,
}

#[derive(Debug, Deserialize_repr)]
#[repr(u8)]
pub enum GuildNsfwLevel {
    Default = 0,
    Explicit = 1,
    Safe = 2,
    AgeRestricted = 3,
}

#[derive(Debug, Deserialize_repr)]
#[repr(u8)]
pub enum PremiumTier {
    None = 0,
    Level1 = 1,
    Level2 = 2,
    Level3 = 3,
}

#[derive(Debug, Deserialize_repr)]
#[repr(u8)]
pub enum SystemChannelFlags {
    SuppressJoinNotifications = 0,
    SuppressPremiumSubscriptions = 1,
    SuppressGuildReminderNotifications = 2,
    SuppressJoinNotificationReplies = 3,
    SuppressRoleSubscriptionPurchaseNotifications = 4,
    SuppressRoleSubscriptionPurchaseNotificationReplies = 5,
}

#[derive(Debug, Serialize, Deserialize)]
pub enum GuildFeature {
    #[serde(rename = "ANIMATED_BANNER")]
    AnimatedBanner,
    #[serde(rename = "ANIMATED_ICON")]
    AnimatedIcon,
    #[serde(rename = "APPLICATION_COMMAND_PERMISSIONS_V2")]
    ApplicationCommandPermissionsV2,
    #[serde(rename = "AUTO_MODERATION")]
    AutoModeration,
    #[serde(rename = "BANNER")]
    Banner,
    #[serde(rename = "COMMUNITY")]
    Community,
    #[serde(rename = "CREATOR_MONETIZABLE_PROVISIONAL")]
    CreatorMonetizableProvisional,
    #[serde(rename = "CREATOR_STORE_PAGE")]
    CreatorStorePage,
    #[serde(rename = "DEVELOPER_SUPPORT_SERVER")]
    DeveloperSupportServer,
    #[serde(rename = "DISCOVERABLE")]
    Discoverable,
    #[serde(rename = "FEATURABLE")]
    Featurable,
    #[serde(rename = "INVITES_DISABLED")]
    InvitesDisabled,
    #[serde(rename = "INVITE_SPLASH")]
    InviteSplash,
    #[serde(rename = "MEMBER_VERIFICATION_GATE_ENABLED")]
    MemberVerificationGateEnabled,
    #[serde(rename = "MORE_STICKERS")]
    MoreStickers,
    #[serde(rename = "NEWS")]
    News,
    #[serde(rename = "PARTNERED")]
    Partnered,
    #[serde(rename = "PREVIEW_ENABLED")]
    PreviewEnabled,
    #[serde(rename = "RAID_ALERTS_DISABLED")]
    RaidAlertsDisabled,
    #[serde(rename = "ROLE_ICONS")]
    RoleIcons,
    #[serde(rename = "ROLE_SUBSCRIPTIONS_AVAILABLE_FOR_PURCHASE")]
    RoleSubscriptionsAvailableForPurchase,
    #[serde(rename = "ROLE_SUBSCRIPTIONS_ENABLED")]
    RoleSubscriptionsEnabled,
    #[serde(rename = "TICKETED_EVENTS_ENABLED")]
    TicketedEventsEnabled,
    #[serde(rename = "VANITY_URL")]
    VanityUrl,
    #[serde(rename = "VERIFIED")]
    Verified,
    #[serde(rename = "VIP_REGIONS")]
    VipRegions,
    #[serde(rename = "WELCOME_SCREEN_ENABLED")]
    WelcomeScreenEnabled,
    #[serde(rename = "TEXT_IN_VOICE_ENABLED")]
    TextInVoiceEnabled,
}

#[derive(Debug, Deserialize)]
pub enum MutableGuildFeature {
    #[serde(rename = "COMMUNITY")]
    Community,
    #[serde(rename = "DISCOVERABLE")]
    Discoverable,
    #[serde(rename = "INVITES_DISABLED")]
    InvitesDisabled,
    #[serde(rename = "RAID_ALERTS_DISABLED")]
    RaidAlertsDisabled,
}

#[derive(Debug, Deserialize)]
pub struct GuildPreview {
    pub id: String,
    pub name: String,
    pub icon: Option<String>,
    pub splash: Option<String>,
    pub discovery_splash: Option<String>,
    pub emojis: Vec<Emoji>,
    pub features: Vec<GuildFeature>,
    pub approximate_member_count: i32,
    pub approximate_presence_count: i32,
    pub description: Option<String>,
    pub stickers: Vec<Sticker>,
}

#[derive(Debug, Deserialize)]
pub struct GuildWidgetSetttings {
    pub enabled: bool,
    pub channel_id: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct GuildWidget {
    pub id: String,
    pub name: String,
    pub instant_invite: Option<String>,
    pub channels: Vec<PartialChannel>,
    pub members: Vec<PartialUser>,
    pub presence_count: i32,
}

#[derive(Debug, Deserialize, Partial)]
pub struct GuildMember {
    pub user: Option<User>,
    pub nick: Option<String>,
    pub avatar: Option<String>,
    pub roles: Vec<String>,
    pub joined_at: u32,
    pub premium_since: Option<u32>,
    pub deaf: bool,
    pub mute: bool,
    pub flags: i32,
    pub pending: Option<bool>,
    pub permissions: Option<String>,
    pub communication_disabled_until: Option<u32>,
}

#[derive(Debug, Deserialize_repr)]
#[repr(u8)]
pub enum GuildMemberFlag {
    DidRejoin = 0,
    CompletedOnboarding = 1,
    BypassesVerification = 2,
    StartedOnboarding = 3,
}

#[derive(Debug, Deserialize, Partial)]
pub struct Integration {
    pub id: String,
    pub name: String,
    #[serde(rename = "type")]
    pub integration_type: String,
    pub enabled: bool,
    pub syncing: Option<bool>,
    pub role_id: Option<String>,
    pub enable_emoticons: Option<bool>,
    pub expire_behavior: Option<IntegrationExpireBehavior>,
    pub expire_grace_period: Option<i32>,
    pub user: Option<User>,
    pub account: IntegrationAccount,
    pub synced_at: u32,
    pub subscriber_count: Option<i32>,
    pub revoked: Option<bool>,
    pub application: Option<Application>,
    //pub scopes?: array of OAuth2 scopes
}

#[derive(Debug, Deserialize_repr)]
#[repr(u8)]
pub enum IntegrationExpireBehavior {
    RemoveRole = 0,
    Kick = 1,
}

#[derive(Debug, Deserialize)]
pub struct IntegrationAccount {
    pub id: String,
    pub name: String,
}

#[derive(Debug, Deserialize)]
pub struct IntegrationApplication {
    pub id: Sticker,
    pub name: String,
    pub icon: Option<String>,
    pub description: String,
    pub bot: Option<User>,
}

#[derive(Debug, Deserialize)]
pub struct Ban {
    pub reason: Option<String>,
    pub user: User,
}

#[derive(Debug, Deserialize)]
pub struct WelcomeScreen {
    pub description: Option<String>,
    pub welcome_channels: Vec<WelcomeScreenChannel>,
}

#[derive(Debug, Deserialize)]
pub struct WelcomeScreenChannel {
    pub channel_id: String,
    pub description: String,
    pub emoji_id: Option<String>,
    pub emoji_name: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct GuildOnboarding {
    pub guild_id: String,
    pub prompts: Vec<OnboardingPrompt>,
    pub default_channel_ids: Vec<String>,
    pub enabled: bool,
    pub mode: OnboardingMode,
}

#[derive(Debug, Deserialize)]
pub struct OnboardingPrompt {
    pub id: String,
    #[serde(rename = "type")]
    pub prompt_type: PromptType,
    pub options: Vec<PromptOption>,
    pub title: String,
    pub single_select: bool,
    pub required: bool,
    pub in_onboarding: bool,
}

#[derive(Debug, Deserialize)]
pub struct PromptOption {
    pub id: String,
    pub channel_ids: Vec<String>,
    pub role_ids: Vec<String>,
    pub emoji: Emoji,
    pub title: String,
    pub description: Option<String>,
}

#[derive(Debug, Deserialize_repr)]
#[repr(u8)]
pub enum OnboardingMode {
    OnboardingDefault = 0,
    OnboardingAdvanced = 1,
}

#[derive(Debug, Deserialize_repr)]
#[repr(u8)]
pub enum PromptType {
    MultipleChoice = 0,
    Dropdown = 1,
}
