use super::prelude::*;
use serde::{Deserialize, Serialize};
use serde_repr::{Deserialize_repr, Serialize_repr};
use std::collections::HashMap;

#[derive(Debug, Serialize, Deserialize)]
pub struct ApplicationCommand {
    pub id: String,
    #[serde(rename = "type")]
    pub command_type: Option<ApplicationCommandType>,
    pub application_id: String,
    pub guild_id: Option<String>,
    pub name: String,
    pub name_localizations: Option<HashMap<Locale, String>>,
    pub description: String,
    pub description_localizations: Option<HashMap<Locale, String>>,
    pub options: Option<Vec<ApplicationCommandOption>>,
    pub default_member_permissions: Option<String>,
    pub dm_permission: Option<bool>,
    pub nsfw: Option<bool>,
    pub version: String,
}

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum ApplicationCommandType {
    ChatInput = 1,
    User = 2,
    Message = 3,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ApplicationCommandOption {
    #[serde(rename = "type")]
    pub command_type: ApplicationCommandType,
    pub name: String,
    pub name_localizations: Option<HashMap<Locale, String>>,
    pub description: String,
    pub description_localizations: Option<HashMap<Locale, String>>,
    pub required: Option<bool>,
    pub choices: Option<Vec<ApplicationCommandOptionChoice>>,
    pub options: Option<Vec<ApplicationCommandOption>>,
    pub channel_types: Option<Vec<ChannelType>>,
    pub min_value: Option<i16>,
    pub max_value: Option<u16>,
    pub min_length: Option<u16>,
    pub max_length: Option<u16>,
    pub autocomplete: Option<bool>,
}

#[derive(Debug, Deserialize_repr)]
#[repr(u8)]
pub enum ApplicationCommandOptionType {
    SubCommand = 1,
    SubCommandGroup = 2,
    String = 3,
    Integer = 4,
    Boolean = 5,
    User = 6,
    Channel = 7,
    Role = 8,
    Mentionable = 9,
    Number = 10,
    Attachment = 11,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ApplicationCommandOptionChoice {
    pub name: String,
    pub name_localizations: Option<HashMap<Locale, String>>,
    pub value: ApplicationCommandOptionChoiceValue,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApplicationCommandOptionChoiceValue {
    String(String),
    Int(isize),
}

#[derive(Debug, Deserialize)]
pub struct GuildApplicationCommandPermission {
    pub id: String,
    pub application_id: String,
    pub guild_id: String,
    pub permissions: Vec<ApplicationCommandPermission>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ApplicationCommandPermission {
    pub id: String,
    #[serde(rename = "type")]
    pub permission_type: ApplicationCommandPermissionType,
    pub permission: bool,
}

#[derive(Debug, Serialize_repr, Deserialize_repr)]
#[repr(u8)]
pub enum ApplicationCommandPermissionType {
    Role = 1,
    User = 2,
    Channel = 3,
}
