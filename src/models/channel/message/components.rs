use crate::models::{from_map, prelude::*};
use discordinal_derive::Setters;
use serde::{ser::SerializeStruct, Deserialize, Serialize};
use serde_repr::{Deserialize_repr, Serialize_repr};

/// Use when calculation struct length when implementing [`serde::Serialize`]
macro_rules! decrement_if_none {
    ($self:ident, $key:ident, $i:ident) => {
        if $self.$key.is_none() {
            $i += 1;
        }
    };
}

/// Use when implementing [`serde::Serialize`]
macro_rules! serialize_if_some {
    ($struct:ident, $self:ident, $key:ident) => {
        if $self.$key.is_some() {
            $struct.serialize_field(stringify!($key), &$self.$key)?;
        }
    };
}

/// Represents all possible action row types
///
/// Since an action row can only contain one type of component
/// at a time, for easier to work with typings, action rows
/// are separated into two types: ButtonActionRow and SelectMenuActionRow.
///
/// This enum exist to allow matching which type of action row it is.
///
/// # Examples
///
/// Creating
///
/// ```
/// use discordinal::models::prelude::*,
///
/// let button = Button::default()
///     .style(ButtonStyle::Link)
///     .url("example.com");
///
/// let action_row = ActionRow::ButtonRow(vec![button]);
/// ```
///
/// ```
/// use discordinal::models::prelude::*;
///
/// let select_menu = SelectMenu::default()
///     .custom_id("id")
///     .options(vec![]); // some options here
///
/// let action_row = ActionRow::SelectMenuRow(select_menu);
/// ```
///
/// Accessing
///
/// ```ignore
/// if let Some(rows) = message.components {
///     for row in rows {
///         match row {
///             ActionRow::ButtonRow(buttons) => {},
///             ActionRow::SelectMenuRow(select_menu) => {},
///         }
///     }
/// }
/// ```
#[derive(Debug)]
pub enum ActionRow {
    ButtonRow(Vec<Button>),
    SelectMenuRow(SelectMenu),
}

// Deserialize based on the type of component inside the action row
impl<'de> Deserialize<'de> for ActionRow {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        use serde::de::Error;
        use serde_json::{from_value, Map, Value};

        let mut map = Map::deserialize(deserializer)?;
        let components = from_map!(map, "components", "", Vec<Value>);

        // An ActionRow is not expected to be empty, to accessing index 0 should be fine
        let component_type = components[0]["type"]
            .as_i64()
            .ok_or(Error::custom("ActionRow does not contain any components"))?;

        match component_type {
            2 => {
                let buttons = components
                    .into_iter()
                    .map(|v| from_value(v).map_err(Error::custom))
                    .collect::<Result<Vec<_>, _>>();
                Ok(Self::ButtonRow(buttons?))
            }
            3 | 5..=8 => {
                let select_menu = from_value(components[0].clone()).map_err(Error::custom)?;
                Ok(Self::SelectMenuRow(select_menu))
            }
            _ => unreachable!(),
        }
    }
}

// When serializing, add the "type" field with value 1
impl Serialize for ActionRow {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        use serde::ser::Error;
        use serde_json::{json, to_value, Value};

        let row: Value = match self {
            Self::ButtonRow(row) => to_value(row).map_err(Error::custom)?,
            Self::SelectMenuRow(row) => to_value(row).map_err(Error::custom)?,
        };

        json!({
            "type": 1,
            "components": row["components"],
        })
        .serialize(serializer)
    }
}

/// Discord Button component
///
/// # Examples
///
/// There are two button types:
///
/// 1. Regular button (must have [`url`](Button::url) set to [`None`] and all other fields set to [`Some`] and [`style`](Button::style) not set to [`ButtonStyle::Link`])
///     ```
///     use discordinal::models::prelude::*;
///
///     Button::default()
///         .label("click me")
///         .custom_id("id")
///         .disabled(false);
///     ```
/// 2. Url button (must have [`url`](Button::url), [`style`](Button::style) set to [`ButtonStyle::Link`] and all other fields set to [`None`])
///     ```
///     use discordinal::models::prelude::*;
///
///     Button::default()
///         .style(ButtonStyle::Link)
///         .url("example.com");
///     ```
#[derive(Debug, Default, Setters, Deserialize)]
pub struct Button {
    pub style: ButtonStyle,
    pub label: Option<String>,
    pub emoji: Option<Emoji>,
    pub custom_id: Option<String>,
    pub url: Option<String>,
    pub disabled: Option<bool>,
}

// Add "type" field with value 2 when serializing
impl Serialize for Button {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        // Since we don't want to serialize fields where the value is None
        // (according to Discord API documentation), the large amount of
        // extra code in this function is for skipping serialization

        let mut len = 6;

        decrement_if_none!(self, label, len);
        decrement_if_none!(self, custom_id, len);
        decrement_if_none!(self, url, len);
        decrement_if_none!(self, disabled, len);

        let mut button = serializer.serialize_struct("Button", len)?;

        button.serialize_field("type", &2)?;
        button.serialize_field("style", &self.style)?;
        serialize_if_some!(button, self, label);
        serialize_if_some!(button, self, custom_id);
        serialize_if_some!(button, self, url);
        serialize_if_some!(button, self, disabled);
        button.end()
    }
}

#[derive(Debug, Default, Deserialize_repr, Serialize_repr)]
#[repr(i8)]
pub enum ButtonStyle {
    #[default]
    Primary = 1,
    Secondary = 2,
    Success = 3,
    Danger = 4,
    Link = 5,
}

/// Discord SelectMenu component
///
/// # Examples
///
/// String select menu
///
/// ```
/// use discordinal::models::prelude::*;
///
/// SelectMenu::default()
///     .select_menu_type(SelectMenuType::Text)
///     .custom_id("id")
///     .options(vec![]);
///     // ...use other setters
/// ```
#[derive(Debug, Default, Setters, Deserialize, Serialize)]
pub struct SelectMenu {
    #[serde(rename = "type")]
    pub select_menu_type: SelectMenuType,
    pub custom_id: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub options: Option<Vec<SelectMenuOption>>,
    pub channel_types: Option<Vec<ChannelType>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub placeholder: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub min_values: Option<i16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub max_values: Option<i16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub disabled: Option<bool>,
}

#[derive(Debug, Default, Deserialize_repr, Serialize_repr)]
#[repr(i8)]
pub enum SelectMenuType {
    #[default]
    Text = 3,
    User = 5,
    Role = 6,
    Mentionable = 7,
    Channel = 8,
}

/// SelectMenu option
///
/// # Examples
///
/// ```
/// use discordinal::models::prelude::*;
///
/// SelectMenuOption::default()
///     .label("example")
///     .value("example");
///     // ...use other setters
/// ```
#[derive(Debug, Default, Setters, Deserialize, Serialize)]
pub struct SelectMenuOption {
    pub label: String,
    pub value: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub emoji: Option<PartialEmoji>,
    #[serde(rename = "default")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub selected_by_default: Option<bool>,
}

/// Discord TextInput component
///
/// # Examples
///
/// ```
/// use discordinal::models::prelude::*;
///
/// TextInput::default()
///     .style(TextInputStyle::Paragraph) // defaults to Short
///     .custom_id("id")
///     .label("some label");
///     // ...use other setters
/// ```
#[derive(Debug, Default, Setters, Deserialize)]
pub struct TextInput {
    pub custom_id: String,
    pub style: TextInputStyle,
    pub label: String,
    pub min_length: Option<i32>,
    pub max_length: Option<i32>,
    pub required: Option<bool>,
    pub value: Option<String>,
    pub placeholder: Option<String>,
}

impl Serialize for TextInput {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut len = 9;

        decrement_if_none!(self, min_length, len);
        decrement_if_none!(self, max_length, len);
        decrement_if_none!(self, required, len);
        decrement_if_none!(self, value, len);
        decrement_if_none!(self, placeholder, len);

        let mut input = serializer.serialize_struct("TextInput", len)?;

        input.serialize_field("type", &4)?;
        input.serialize_field("custom_id", &self.custom_id)?;
        input.serialize_field("style", &self.style)?;
        input.serialize_field("label", &self.label)?;
        serialize_if_some!(input, self, min_length);
        serialize_if_some!(input, self, max_length);
        serialize_if_some!(input, self, required);
        serialize_if_some!(input, self, value);
        serialize_if_some!(input, self, placeholder);

        input.end()
    }
}

#[derive(Debug, Default, Deserialize_repr, Serialize_repr)]
#[repr(i8)]
pub enum TextInputStyle {
    #[default]
    Short = 1,
    Paragraph = 2,
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde::Serialize;

    #[test]
    fn serialize_with_type() {
        fn serialized_type<T: Serialize>(v: &T) -> Option<i64> {
            serde_json::to_value(v).ok()?.get("type")?.as_i64()
        }

        let btn = Button::default();
        assert_eq!(Some(2), serialized_type(&btn));

        let inp = TextInput::default();
        assert_eq!(Some(4), serialized_type(&inp));

        let ac = ActionRow::ButtonRow(vec![btn]);
        assert_eq!(Some(1), serialized_type(&ac));
    }

    #[test]
    fn deserialize_action_row_based_on_type() {
        let json = serde_json::json!({
            "type": 1,
            "components": [{
                "type": 2,
                "style": 1,
            }]
        });

        assert!(matches!(
            serde_json::from_value(json).unwrap(),
            ActionRow::ButtonRow(..)
        ));

        let json = serde_json::json!({
            "type": 1,
            "components": [{
                "type": 3,
                "custom_id": "id",
            }]
        });

        assert!(matches!(
            serde_json::from_value(json).unwrap(),
            ActionRow::SelectMenuRow(..)
        ));
    }
}
