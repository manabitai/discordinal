use discordinal_derive::Setters;
use serde::{Deserialize, Serialize};

/// Discord Message Embed
///
/// # Embed Limits
///
/// To facilitate showing rich content,
/// rich embeds do not follow the traditional limits of message content.
/// However, some limits are still in place to prevent excessively large embeds.
/// The following table describes the limits:
///
/// All of the following limits are measured inclusively.
/// Leading and trailing whitespace characters are not included
/// (they are trimmed automatically).
///
/// | Field       | Limit                  |
/// | ----------- | ---------------------- |
/// | title       | 256 characters         |
/// | description | 4096 characters        |
/// | fields      | Up to 25 field objects |
/// | field.name  | 256 characters         |
/// | field.value | 1024 characters        |
/// | footer.text | 2048 characters        |
/// | author.name | 256 characters         |
///
/// Additionally, the combined sum of characters in all title, description,
/// field.name, field.value, footer.text, and author.name fields across all
/// embeds attached to a message must not exceed 6000 characters.
/// Violating any of these constraints will result in a Bad Request response.
///
/// Embeds are deduplicated by URL. If a message contains multiple embeds with the same URL, only the first is shown.
#[derive(Debug, Default, Setters, Deserialize, Serialize)]
pub struct Embed {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub timestamp: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub color: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub footer: Option<Footer>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub image: Option<Image>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub thumbnail: Option<Thumbnail>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub video: Option<Video>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub provider: Option<Provider>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub author: Option<Author>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<Vec<Field>>,
}

#[derive(Debug, Default, Setters, Deserialize, Serialize)]
pub struct Footer {
    pub text: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub icon_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub proxy_icon_url: Option<String>,
}

#[derive(Debug, Default, Setters, Deserialize, Serialize)]
pub struct Image {
    pub url: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub proxy_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub height: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub width: Option<i32>,
}

#[derive(Debug, Default, Setters, Deserialize, Serialize)]
pub struct Thumbnail {
    pub url: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub proxy_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub height: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub width: Option<i32>,
}

#[derive(Debug, Default, Setters, Deserialize, Serialize)]
pub struct Video {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub proxy_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub height: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub width: Option<i32>,
}

#[derive(Debug, Default, Setters, Deserialize, Serialize)]
pub struct Provider {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
}

#[derive(Debug, Default, Setters, Deserialize, Serialize)]
pub struct Author {
    pub name: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub icon_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub proxy_icon_url: Option<String>,
}

#[derive(Debug, Default, Setters, Deserialize, Serialize)]
pub struct Field {
    pub name: String,
    pub value: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub inline: Option<bool>,
}
