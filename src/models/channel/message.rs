pub mod components;
pub mod embeds;

use crate::models::prelude::*;
use discordinal_derive::{partial_serialize, Partial};
use serde::{Deserialize, Serialize};
use serde_repr::Deserialize_repr;

#[derive(Debug, Deserialize_repr)]
#[repr(u8)]
pub enum MessageType {
    Default = 0,
    RecipientAdd = 1,
    RecipientRemove = 2,
    Call = 3,
    ChannelNameChange = 4,
    ChannelIconChange = 5,
    ChannelPinnedMessage = 6,
    UserJoin = 7,
    GuildBoost = 8,
    GuildBoostTier1 = 9,
    GuildBoostTier2 = 10,
    GuildBoostTier3 = 11,
    ChannelFollowAdd = 12,
    GuildDiscoveryDisqualified = 14,
    GuildDiscoveryRequalified = 15,
    GuildDiscoveryGracePeriodInitialWarning = 16,
    GuildDiscoveryGracePeriodFinalWarning = 17,
    ThreadCreated = 18,
    Reply = 19,
    ChatInputCommand = 20,
    ThreadStarterMessage = 21,
    GuildInviteReminder = 22,
    ContextMenuCommand = 23,
    AutoModerationAction = 24,
}

#[derive(Debug, Deserialize, Partial)]
pub struct Message {
    pub id: String,
    pub channel_id: String,
    pub author: User,
    pub content: Option<String>,
    pub timestamp: String,
    pub edited_timestamp: Option<String>,
    pub tts: bool,
    pub mention_everyone: bool,
    pub mentions: Vec<User>,
    pub mention_roles: Vec<Role>,
    pub mention_channels: Option<Vec<Channel>>,
    pub attachments: Vec<Attachment>,
    pub embeds: Vec<Embed>,
    pub reactions: Option<Vec<Reaction>>,
    pub nonce: Option<String>,
    pub pinned: bool,
    pub webhook_id: Option<String>,
    #[serde(rename = "type")]
    pub message_type: MessageType,
    pub activity: Option<Activity>,
    pub application: Option<Application>,
    pub application_id: Option<String>,
    pub message_reference: Option<Reference>,
    pub flags: Option<i32>,
    pub referenced_message: Option<Box<Message>>,
    pub interaction: Option<MessageInteraction>,
    pub thread: Option<Channel>,
    pub components: Option<Vec<ActionRow>>,
    pub sticker_items: Option<Vec<StickerItem>>,
    pub stickers: Option<Vec<Sticker>>,
    pub position: Option<i32>,
}

#[derive(Debug, Deserialize)]
pub struct Activity {
    #[serde(rename = "type")]
    pub activity_type: ActivityType,
    pub party_id: Option<String>,
}

#[derive(Debug, Deserialize_repr)]
#[repr(i8)]
pub enum ActivityType {
    Join = 1,
    Spectate = 2,
    Listen = 3,
    JoinRequest = 4,
}

#[derive(Debug, Deserialize, Partial)]
#[partial_serialize]
pub struct Attachment {
    pub id: String,
    pub filename: String,
    pub description: Option<String>,
    pub content_type: Option<String>,
    pub size: i32,
    pub url: String,
    pub proxy_url: String,
    pub height: Option<i32>,
    pub width: Option<i32>,
    pub ephemeral: Option<bool>,
}

#[derive(Debug, Deserialize)]
pub struct Reference {
    pub message_id: Option<String>,
    pub channel_id: Option<String>,
    pub guild_id: Option<String>,
    pub fail_if_not_exists: Option<bool>,
}

#[derive(Debug, Deserialize)]
pub struct MessageInteraction {
    #[serde(rename = "type")]
    pub interaction_type: InteractionType,
    /// ID of the interaction
    pub id: String,
    /// Name of the application command, including subcommands and subcommand groups
    pub name: String,
    /// User who invoked the interaction
    pub user: User,
    pub member: Option<PartialGuildMember>,
}

#[derive(Debug, Deserialize_repr)]
#[repr(i8)]
pub enum InteractionType {
    Ping = 1,
    ApplicationCommand = 2,
    MessageComponent = 3,
    ApplicationCommandAutocomplete = 4,
    ModalSubmit = 5,
}
