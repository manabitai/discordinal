use serde::{Deserialize, Serialize};

pub mod application;
pub mod application_command;
pub mod application_role_connection_metadata;
pub mod channel;
pub mod codes;
pub mod emoji;
pub mod guild;
pub mod payload;
pub mod sticker;
pub mod team;
pub mod user;

macro_rules! from_map {
    ($map:ident, $name:expr, $err:expr, $type:ty) => {
        $map.remove($name)
            .ok_or_else(|| serde::de::Error::custom($err))
            .and_then(<$type>::deserialize)
            .map_err(serde::de::Error::custom)?
    };
}

pub(crate) use from_map;

pub mod prelude {
    pub use super::{
        application::*,
        application_command::*,
        application_role_connection_metadata::*,
        channel::{
            message::{components::*, embeds::*, *},
            *,
        },
        emoji::*,
        guild::{role::*, *},
        sticker::*,
        team::*,
        user::*,
        Locale,
    };
}

#[derive(Debug, PartialEq, Eq, Hash, Deserialize, Serialize)]
pub enum Locale {
    #[serde(rename = "id")]
    Id,
    #[serde(rename = "da")]
    Da,
    #[serde(rename = "de")]
    De,
    #[serde(rename = "en-GB")]
    EnGb,
    #[serde(rename = "en-US")]
    EnUs,
    #[serde(rename = "es-ES")]
    EsEs,
    #[serde(rename = "fr")]
    Fr,
    #[serde(rename = "hr")]
    Hr,
    #[serde(rename = "it")]
    It,
    #[serde(rename = "lt")]
    Lt,
    #[serde(rename = "hu")]
    Hu,
    #[serde(rename = "nl")]
    Nl,
    #[serde(rename = "no")]
    No,
    #[serde(rename = "pl")]
    Pl,
    #[serde(rename = "pt-BR")]
    PtBr,
    #[serde(rename = "ro")]
    Ro,
    #[serde(rename = "fi")]
    Fi,
    #[serde(rename = "sv-SE")]
    SvSe,
    #[serde(rename = "vi")]
    Vi,
    #[serde(rename = "tr")]
    Tr,
    #[serde(rename = "cs")]
    Cs,
    #[serde(rename = "el")]
    El,
    #[serde(rename = "bg")]
    Bg,
    #[serde(rename = "ru")]
    Ru,
    #[serde(rename = "uk")]
    Uk,
    #[serde(rename = "hi")]
    Hi,
    #[serde(rename = "th")]
    Th,
    #[serde(rename = "zh-CN")]
    ZhCn,
    #[serde(rename = "ja")]
    Ja,
    #[serde(rename = "zh-TW")]
    ZhTw,
    #[serde(rename = "ko")]
    Ko,
}
