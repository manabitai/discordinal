use super::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct Team {
    pub icon: Option<String>,
    pub id: String,
    pub members: Vec<TeamMember>,
    pub name: String,
    pub owner_user_id: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct TeamMember {
    pub membership_state: MembersipState,
    pub permissions: Vec<String>,
    pub team_id: String,
    pub user: Vec<PartialUser>,
}

#[derive(Debug, Deserialize, Serialize)]
pub enum MembersipState {
    Invited = 1,
    Accepted = 2,
}
