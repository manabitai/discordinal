use self::ready::Ready;
use super::{codes::Opcode, from_map, prelude::*};
use serde::Deserialize;

pub mod ready {
    use crate::models::prelude::*;
    use serde::Deserialize;

    /// The ready event is dispatched when a client has completed the initial
    /// handshake with the gateway (for new sessions). The ready event can be
    /// the largest and most complex event the gateway will send, as it
    /// contains all the state required for a client to begin interacting
    /// with the rest of the platform.
    ///
    /// [`Ready::guilds`] are the guilds of which your bot is a member.
    /// They start out as unavailable when you connect to the gateway.
    /// As they become available, your bot will be notified via Guild Create events.
    #[derive(Debug, Deserialize)]
    pub struct Ready {
        pub v: i8,
        pub user: User,
        pub guilds: Vec<UnavailableGuild>,
        pub session_id: String,
        pub resume_gateway_url: String,
        pub shard: Option<[i8; 2]>,
        pub application: PartialApplication,
    }

    /// A partial [guild][1] object. Represents an Offline Guild,
    /// or a Guild whose information has not been provided through
    /// [Guild Create][2] events during the Gateway connect.
    ///
    /// [1]: https://discord.com/developers/docs/resources/guild#guild-object
    /// [2]: https://discord.com/developers/docs/topics/gateway-events#guild-create
    #[derive(Debug, Deserialize)]
    pub struct UnavailableGuild {
        pub id: String,
        pub unavailable: bool,
    }
}

#[derive(Debug)]
pub(crate) enum Payload {
    Dispatch(i64, Box<Dispatch>),
    Heartbeat,
    /// `max_concurrency` limit exceeded.
    /// `bool` represents whether the session may be resumable.
    InvalidSession(bool),
    Hello {
        heartbeat_interval: i32,
    },
    HeartbeatACK,
}

impl<'de> Deserialize<'de> for Payload {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let mut map = serde_json::Map::deserialize(deserializer)?;
        let op = from_map!(map, "op", "expected op", Opcode);

        Ok(match op {
            Opcode::Dispatch => {
                let v = serde_json::Value::from(map.clone());
                let s = from_map!(map, "s", "expected payload s", i64);
                Self::Dispatch(s, serde_json::from_value(v).unwrap())
            }
            Opcode::Heartbeat => Self::Heartbeat,
            Opcode::InvalidSession => {
                let d = from_map!(map, "d", "expected payload d", bool);
                Self::InvalidSession(d)
            }
            Opcode::Hello => {
                let mut d = from_map!(map, "d", "expected payload d", serde_json::Map<String, serde_json::Value>);
                let heartbeat_interval =
                    from_map!(d, "heartbeat_interval", "expected heartbeat_interval", i32);
                Self::Hello { heartbeat_interval }
            }
            Opcode::HeartbeatACK => Self::HeartbeatACK,
            _ => unimplemented!(),
        })
    }
}

#[derive(Debug, Deserialize)]
#[serde(tag = "t", content = "d")]
pub enum Dispatch {
    #[serde(rename = "READY")]
    Ready(Box<Ready>),
    #[serde(rename = "RESUMED")]
    Resumed,
    #[serde(rename = "MESSAGE_CREATE")]
    MessageCreate(Message),
    #[serde(rename = "MESSAGE_UPDATE")]
    MessageUpdate(Message),
    #[serde(rename = "MESSAGE_DELETE")]
    MessageDelete(PartialMessage),
    #[serde(rename = "CHANNEL_CREATE")]
    ChannelCreate(Box<Channel>),
    #[serde(rename = "CHANNEL_UPDATE")]
    ChannelUpdate(Box<Channel>),
    #[serde(rename = "CHANNEL_DELETE")]
    ChannelDelete(Box<PartialChannel>),
    // TODO need to add newly_created field to Channel https://discord.com/developers/docs/topics/gateway-events#thread-create
    #[serde(rename = "THREAD_CREATE")]
    ThreadCreate(Box<Channel>),
    #[serde(rename = "THREAD_UPDATE")]
    ThreadUpdate(Box<Channel>),
    #[serde(rename = "THREAD_DELETE")]
    ThreadDelete(Box<PartialChannel>),
}
