use serde_repr::Deserialize_repr;

#[derive(Debug, Deserialize_repr)]
#[repr(i8)]
pub enum Opcode {
    Dispatch = 0,
    Heartbeat = 1,
    Identify = 2,
    PresenceUpdate = 3,
    VoiceStateUpdate = 4,
    Resume = 6,
    Reconnect = 7,
    RequestGuildMembers = 8,
    InvalidSession = 9,
    Hello = 10,
    HeartbeatACK = 11,
}

#[derive(Debug, Deserialize_repr, FromPrimitive)]
#[repr(i16)]
pub enum CloseEventCode {
    UnknownError = 4000,
    UnknownOpcode = 4001,
    DecodeError = 4002,
    NotAuthenticated = 4003,
    AuthenticationFailed = 4004,
    AlreadyAuthenticated = 4005,
    InvalidSeq = 4007,
    RateLimited = 4008,
    SessionTimedOut = 4009,
    InvalidShard = 4010,
    ShardingRequired = 4011,
    InvalidAPIVersion = 4012,
    InvalidIntents = 4013,
    DisallowedIntents = 4014,
}

impl CloseEventCode {
    pub fn may_reconnect(&self) -> bool {
        match self {
            Self::UnknownError => true,
            Self::UnknownOpcode => true,
            Self::DecodeError => true,
            Self::NotAuthenticated => false,
            Self::AuthenticationFailed => false,
            Self::AlreadyAuthenticated => true,
            Self::InvalidSeq => true,
            Self::RateLimited => true,
            Self::SessionTimedOut => true,
            Self::InvalidShard => false,
            Self::ShardingRequired => false,
            Self::InvalidAPIVersion => false,
            Self::InvalidIntents => false,
            Self::DisallowedIntents => false,
        }
    }
}
